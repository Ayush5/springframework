package com.esewa.college;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

public class Main {
    public static void main(String[] args) {
        BeanFactory beanFactory=new XmlBeanFactory(new FileSystemResource("applicationContext.xml"));
        Student student=(Student)beanFactory.getBean("studentbean");
        student.display();
    }
}
